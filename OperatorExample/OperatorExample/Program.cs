﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var json = "{\"Name\":\"Some name\",\"Title\":\"Some title\",\"Description\":\"Something completely nonsensical.\"}";

            BaseEntityGeneric entity = new ConcreteEntityGeneric(json);
            //ConcreteEntity entity = new ConcreteEntity(json);

            JObject jsonObject = entity;
            //BaseEntityGeneric entity1 = (BaseEntityGeneric)jsonObject;

            //Console.WriteLine(jsonObject);

            var success = Send(entity);
            var result = Receive();

            Console.ReadLine();
        }

        public static bool Send(BaseEntityGeneric entity)
        {
            var jsonString = BaseEntityGeneric.ToJson(entity);

            // TODO: Send the serialized object by calling the appropriate endpoint service.

            return true;
        }

        public static BaseEntityGeneric Receive()
        {
            // TODO: Call the appropriate endpoint service to receive the serialized response.
            var json = "{\"Name\":\"Some name\",\"Title\":\"Some title\",\"Description\":\"Something completely nonsensical.\"}";

            var result = BaseEntityGeneric.FromJson(json);

            return result;
        }
    }

    abstract class BaseEntityGeneric
    {
        protected abstract JObject Convert(BaseEntityGeneric entity);

        public static implicit operator JObject(BaseEntityGeneric entity)
        {
            return entity.Convert(entity);
        }

        public static explicit operator BaseEntityGeneric(JObject json)
        {
            /* NOTE: We are fucked. Why? 
                Because we can not delegate to a method overriden in the subclass like above due to the fact that we don't have any information about which subtype of BaseEntityGeneric we actually want to create.*/
            throw new NotImplementedException();
        }

        public static BaseEntityGeneric FromJson(string json)
        {
            var result = JsonConvert.DeserializeObject<ConcreteEntityGeneric>(json);
            return result;
        }

        public static string ToJson(BaseEntityGeneric entity)
        {
            var jsonString = JsonConvert.SerializeObject(entity);
            return jsonString;
        }
    }

    class ConcreteEntity
    {
        public string _name;
        public string _title;
        public string _description;
        
        public ConcreteEntity(string jsonString)
        {
            var json = JObject.Parse(jsonString);

            _name = json.Value<string>("Name");
            _title = json.Value<string>("Title");
            _description = json.Value<string>("Description");
        }

        public ConcreteEntity(string name, string title, string description)
        {
            _name = name;
            _title = title;
            _description = description;
        }
    }

    class ConcreteEntityGeneric : BaseEntityGeneric
    {
        private string _name;
        private string _title;
        private string _description;
        private JObject _json;

        public string Name { get { return this._name; } set { this._name = value; } }
        public string Title { get { return this._title; } set { this._title = value; } }
        public string Description { get { return this._description; } set { this._description = value; } }

        [JsonConstructor]
        public ConcreteEntityGeneric() { }

        public ConcreteEntityGeneric(string jsonString)
        {
            _json = JObject.Parse(jsonString);

            _name = _json.Value<string>("Name");
            _title = _json.Value<string>("Title");
            _description = _json.Value<string>("Description");
        }

        public ConcreteEntityGeneric(string name, string title, string description)
        {
            _name = name;
            _title = title;
            _description = description;
        }

        protected override JObject Convert(BaseEntityGeneric entity)
        {
            if (((ConcreteEntityGeneric)entity)._json != null)
            {
                return ((ConcreteEntityGeneric)entity)._json;
            }

            return new JObject
                {
                    { "Name", ((ConcreteEntityGeneric)entity)._name },
                    { "Title", ((ConcreteEntityGeneric)entity)._title },
                    { "Description", ((ConcreteEntityGeneric)entity)._description }
                };
        }

        // NOTE: The following two operators are implemented in the base class and are only usefull in case no ihheritance exists!
        /*public static implicit operator JObject(ConcreteEntityGeneric entity)
        {
            if (entity._json != null)
            {
                return entity._json;
            }

            return new JObject
            {
                { "name", entity._name },
                { "title", entity._title },
                { "description", entity._description }
            };
        }

        public static explicit operator ConcreteEntityGeneric(JObject json)
        {
            var entity = new ConcreteEntityGeneric(
                json.Value<string>("name"),
                json.Value<string>("title"),
                json.Value<string>("description"));

            return entity;
        }*/
    }
}
